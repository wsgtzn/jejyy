package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ejyy_vistor")
public class Vistor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("community_id")
    private long communityId;

    @JsonProperty("building_id")
    private long buildingId;

    @JsonProperty("wechat_mp_user_id")
    private Long wechatMpUserId;

    @JsonProperty("property_company_user_id")
    private Long propertyCompanyUserId;

    @JsonProperty("vistor_name")
    private String vistorName;

    @JsonProperty("vistor_phone")
    private String vistorPhone;

    @JsonProperty("car_number")
    private String carNumber;

    @JsonProperty("have_vistor_info")
    private int haveVistorInfo;

    @JsonProperty("expire")
    private long expire;

    @JsonProperty("used_at")
    private Long usedAt;

    @JsonProperty("scan_by")
    private Long scanBy;

    @JsonProperty("created_at")
    private long createdAt;
}
