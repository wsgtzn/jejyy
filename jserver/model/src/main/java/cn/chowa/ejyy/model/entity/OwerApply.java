package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @ClassName OwerApply
 * @Description TODO
 * @Author ironman
 * @Date 14:30 2022/8/24
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ejyy_ower_apply")
public class OwerApply {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "微信用户不为空")
    @JsonProperty("wechat_mp_user_id")
    private long wechatMpUserId;

    @NotNull(message = "园区名称不能为空")
    @JsonProperty("community_name")
    private String communityName;

    private String house;
    private String carport;
    private String warehouse;

    @JsonProperty("community_id")
    private long communityId;

    private int subscribed;
    private int replied;
    private String content;

    @JsonProperty("replied_at")
    private Long repliedAt;

    @JsonProperty("replied_by")
    private Long repliedBy;

    private boolean success;

    @JsonProperty("reply_content")
    private String replyContent;

    @JsonProperty("created_at")
    private Long createdAt;








}
