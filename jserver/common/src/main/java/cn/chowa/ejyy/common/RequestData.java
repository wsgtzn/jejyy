package cn.chowa.ejyy.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;

import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

/**
 * 请求参数
 */
public class RequestData extends HashMap<String, Object> {

    public long getId() {
        String id = String.valueOf(get("id"));
        return Convert.toLong(id, 0L);
    }

    public long getCommunityId() {
        String id = String.valueOf(get("community_id"));
        checkParam(id, true, "^\\d+$");
        return Convert.toLong(id);
    }

    public int getPageNum() {
        return getInt("page_num", true, "^\\d+$");
    }

    public int getPageSize() {
        return getInt("page_size", true, "^\\d+$");
    }

    public <T> T getData(Class<T> cls) {
        return BeanUtil.toBeanIgnoreCase(this, cls, true);
    }

    public String getJsonStr(String name) {
        Object obj = get(name);
        return obj != null ? JSONUtil.toJsonStr(obj) : "";
    }

    public String getJsonStr(String name, boolean required) {
        Object obj = get(name);
        String str = obj != null ? JSONUtil.toJsonStr(obj) : "";
        checkParam(str, required, "");
        return str;
    }

    public String getStr(String name) {
        Object obj = get(name);
        return obj == null ? null : String.valueOf(get(name));
    }

    public String getStr(String name, boolean required, String validReg) {
        Object val = get(name);
        if (val == null && !required) {
            return "";
        }
        String strVal = "";
        if (val != null) {
            strVal = val.toString();
        }
        checkParam(strVal, required, validReg);
        return strVal;
    }

    public String getStr(String name, boolean required, int min, int max, String reg) {
        String val = getStr(name, required, reg);
        if (required && (val.length() < min || val.length() > max)) {
            throw new CodeException(Constants.code.PARAMS_ERROR, "参数错误");
        }
        return val;
    }

    public String getStr(String name, boolean required, int min, int max) {
        return getStr(name, required, min, max, null);
    }

    public int getInt(String name) {
        return Convert.toInt(get(name), 0);
    }

    public Integer getInt(String name, boolean required, String validReg) {
        return getInt(name, required, validReg, 0);
    }

    public Integer getInt(String name, boolean required, String validReg, int def) {
        Object val = checkParam(get(name), required, validReg);
        if (val == null) {
            return null;
        }
        return Convert.toInt(val, def);
    }

    public boolean getBool(String name) {
        return getBool(name, false, null);
    }

    public Boolean getBool(String name, boolean required, String validReg) {
        if (get(name) == null) {
            return false;
        }
        Object val = checkParam(get(name), required, validReg);
        if (val instanceof Boolean) {
            return (boolean) val;
        } else if (val instanceof Integer) {
            return ((Integer) val) != 0;
        }
        return false;
    }

    public long getLong(String name) {
        return Convert.toLong(get(name), 0L);
    }

    public Long getLong(String name, boolean required, String validReg) {
        Object val = checkParam(get(name), required, validReg);
        if (val == null) {
            return null;
        }
        return Convert.toLong(val, 0L);
    }

    public float getFloat(String name) {
        return Convert.toFloat(get(name), 0f);
    }

    public Float getFloat(String name, boolean required, String validReg) {
        Object val = checkParam(get(name), required, validReg);
        if (val == null) {
            return null;
        }
        return Convert.toFloat(val, 0f);
    }

    public Boolean getBool(String name, boolean required) {
        Object val = checkParam(get(name), required, null);
        if (val == null) {
            return null;
        }
        return Convert.toBool(val);
    }

    public <T> List<T> getArray(String name) {
        return (List<T>) get(name);
    }

    public <T> List<T> getArray(String name, Function<T, Boolean> valid) {
        List<T> rst = (List<T>) get(name);
        if (rst == null) {
            return null;
        }
        for (T o : rst) {
            if (!valid.apply(o)) {
                throw new CodeException(Constants.code.PARAMS_ERROR, "参数错误");
            }
        }
        return rst;
    }

    private Object checkParam(Object val, boolean required, String validReg) {
        if (val == null && required) {
            throw new CodeException(Constants.code.PARAMS_ERROR, "参数不能为空");
        }

        if (required && StrUtil.isBlank(val.toString())) {
            throw new CodeException(Constants.code.PARAMS_ERROR, "参数不能为空");
        }

        if (val != null && StrUtil.isNotBlank(validReg) && !val.toString().matches(validReg)) {
            throw new CodeException(Constants.code.PARAMS_ERROR, "参数错误");
        }
        return val;
    }

}
