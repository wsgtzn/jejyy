package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;

import java.util.List;

/**
 * @ClassName OwerQuery
 * @Description TODO
 * @Author ironman
 * @Date 14:09 2022/8/23
 */
@JqlQuery
public interface OwerQuery {
    List<ObjData> getOwerApplyList(long community_id, boolean replied,boolean subscribed,boolean success, int page_size, int page_num);
    long getOwerApplyListCount(long community_id, boolean replied,boolean subscribed,boolean success);
    ObjData getOwerApplyDetail(long community_id,long id);
    ObjData getOwerApplyInfo(long community_id,long id);
}
