package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.Fitment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FitmentRepository extends JpaRepository<Fitment, Long> {

    Fitment findByIdAndCommunityIdAndStep(long id, long communityId, int step);

}
