package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.NoticeToUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NoticeToUserRepository extends JpaRepository<NoticeToUser, Long> {

    int countByCommunityId(long communityId);

    List<NoticeToUser> findByCommunityIdAndCreatedAtBetween(long communityId, long start, long end);

}
