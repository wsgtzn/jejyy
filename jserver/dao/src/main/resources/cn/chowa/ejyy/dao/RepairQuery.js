function getBuildingRepair(repair_type,community_id,step,refer,page_size,page_num){
    var sql=`
        select
            a.id,
            a.repair_type,
            a.description,
            a.building_id,
            a.step,
            a.merge_id,
            a.created_at,
            b.type,
            b.area,
            b.building,
            b.unit,
            b.number
        from
            ejyy_repair a left join ejyy_building_info b on a.building_id=b.id
        where
            1=1
            ${repair_type?'and repair_type=:repair_type ':''}
            ${step?(step==4?'and (a.step=:step or a.merge_id is not null)':'and a.step=:step'):''}
            ${refer?(refer=='ower'?'and a.wechat_mp_user_id is not null':'and a.property_company_user_id is not null'):''}
        order by id desc
        limit ${(page_num-1)*page_size},${page_size}
    `;
    return sql;
}

function getBuildingRepairCount(repair_type,community_id,step,refer){
 var sql=`
        select
           count(*)
        from
            ejyy_repair a left join ejyy_building_info b on a.building_id=b.id
        where
            1=1
            ${repair_type?'and repair_type=:repair_type ':''}
            ${step?(step==4?'and (a.step=:step or a.merge_id is not null)':'and a.step=:step'):''}
            ${refer?(refer=='ower'?'and a.wechat_mp_user_id is not null':'and a.property_company_user_id is not null'):''}
    `;
    return sql;
}

function getBuildingRepairInfo(id,community_id){
    var sql=`
        select
           a.id,
           a.wechat_mp_user_id,
           a.property_company_user_id,
           a.repair_type,
           a.building_id,
           a.description,
           a.repair_imgs,
           a.allot_user_id,
           a.alloted_at,
           a.dispose_user_id,
           a.dispose_reply,
           a.dispose_content,
           a.dispose_imgs,
           a.disposed_at,
           a.finished_at,
           a.dispose_subscribed,
           a.confrim_subscribed,
           a.finish_subscribed,
           a.merge_id,
           a.step,
           a.rate,
           a.rate_content,
           a.rated_at,
           a.created_at,
           b.type,
           b.area,
           b.building,
           b.unit,
           b.number
        from ejyy_repair a left join ejyy_building_info b on a.building_id=b.id
        where a.id=:id and a.community_id=:community_id
    `;
    return sql;
}