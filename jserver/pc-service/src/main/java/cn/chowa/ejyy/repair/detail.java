package cn.chowa.ejyy.repair;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.PropertyCompanyUserRepository;
import cn.chowa.ejyy.dao.RepairQuery;
import cn.chowa.ejyy.dao.RepairUrgeRepository;
import cn.chowa.ejyy.dao.WechatMpUserRepository;
import cn.chowa.ejyy.model.entity.PropertyCompanyUser;
import cn.chowa.ejyy.model.entity.WechatMpUser;
import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static cn.chowa.ejyy.common.Constants.code.QUERY_ILLEFAL;
import static cn.chowa.ejyy.common.Constants.repair.ALLOT_REPAIR_STEP;

@RestController("repairDetail")
@RequestMapping("/pc/repair")
public class detail {

    @Autowired
    private RepairQuery repairQuery;

    @Autowired
    private PropertyCompanyUserRepository propertyCompanyUserRepository;

    @Autowired
    private WechatMpUserRepository wechatMpUserRepository;

    @Autowired
    private RepairUrgeRepository repairUrgeRepository;

    /**
     * 维修 - 详情
     */
    @SaCheckRole(Constants.RoleName.WXWF)
    @VerifyCommunity(true)
    @PostMapping("/detail")
    public Map<String, Object> detail(@RequestBody RequestData data) {
        //查询 创建人
        long id = data.getId();
        long community_id = data.getCommunityId();
        ObjData info = repairQuery.getBuildingRepairInfo(id, community_id);
        if (info == null) {
            throw new CodeException(QUERY_ILLEFAL, "非法获取工单信息");
        }

        Map<String, Object> allotInfo = new HashMap<>();
        Map<String, Object> disposedInfo = new HashMap<>();
        Map<String, Object> referInfo;

        int step = info.getInt("step");
        if (step >= ALLOT_REPAIR_STEP) {
            PropertyCompanyUser pcu = propertyCompanyUserRepository.findById(info.getLong("allot_user_id")).get();
            allotInfo = Map.of(
                    "id", pcu.getId(),
                    "real_name", pcu.getRealName()
            );

            pcu = propertyCompanyUserRepository.findById(info.getLong("dispose_user_id")).get();
            disposedInfo = Map.of(
                    "id", pcu.getId(),
                    "real_name", pcu.getRealName()
            );
        }

        Long property_company_user_id = info.getLong("property_company_user_id");
        if (property_company_user_id != null) {
            PropertyCompanyUser pcu = propertyCompanyUserRepository.findById(info.getLong("property_company_user_id")).get();
            referInfo = Map.of(
                    "id", pcu.getId(),
                    "real_name", pcu.getRealName()
            );
        } else {
            WechatMpUser wmu = wechatMpUserRepository.findById(info.getLong("wechat_mp_user_id")).get();
            referInfo = Map.of(
                    "id", wmu.getId(),
                    "real_name", wmu.getRealName()
            );
        }
        long urge_total = repairUrgeRepository.countByRepairId(id);
        info.set("refer", property_company_user_id == null ? "colleague" : "ower");
        info.remove("allot_user_id");
        info.remove("dispose_user_id");
        info.remove("property_company_user_id");
        info.remove("wechat_mp_user_id");

        String repair_imgs = info.getStr("repair_imgs");
        info.put("repair_imgs", repair_imgs != null ? repair_imgs.split("#") : new String[0]);
        String dispose_imgs = info.getStr("dispose_imgs");
        info.put("dispose_imgs", dispose_imgs != null ? dispose_imgs.split("#") : new String[0]);
        return Map.of(
                "info", info,
                "referInfo", referInfo,
                "allotInfo", allotInfo,
                "disposedInfo", disposedInfo,
                "urge_total", urge_total
        );
    }

}
