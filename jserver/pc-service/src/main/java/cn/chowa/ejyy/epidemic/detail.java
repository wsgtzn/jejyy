package cn.chowa.ejyy.epidemic;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.EpidemicQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static cn.chowa.ejyy.common.Constants.code.QUERY_ILLEFAL;

@Slf4j
@RestController("EpidemicDetail")
@RequestMapping("/pc/epidemic")
public class detail {

    @Autowired
    private EpidemicQuery epidemicQuery;

    @SaCheckRole(Constants.RoleName.YQFK)
    @VerifyCommunity(true)
    @PostMapping("/detail")
    public Map<?, ?> getDetail(@RequestBody RequestData data) {
        long id = data.getId();
        long communityId = data.getCommunityId();
        ObjData info = epidemicQuery.getEpidemicDetail(id, communityId);

        if (info == null) {
            throw new CodeException(QUERY_ILLEFAL, "非法查询疫情防控数据");
        }

        return Map.of(
                "info", info
        );
    }

}
