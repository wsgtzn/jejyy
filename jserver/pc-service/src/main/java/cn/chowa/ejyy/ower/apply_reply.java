package cn.chowa.ejyy.ower;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.common.utils.DateTimeUtil;
import cn.chowa.ejyy.common.utils.ToolUtil;
import cn.chowa.ejyy.dao.*;
import cn.chowa.ejyy.model.entity.OwerApply;
import cn.chowa.ejyy.model.entity.UserBuilding;
import cn.chowa.ejyy.model.entity.WechatMpUser;
import cn.chowa.ejyy.service.WeChat;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.hutool.core.date.DateTime;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.User;
import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


/**
 * @ClassName apply_reply
 * @Description TODO
 * @Author ironman
 * @Date 10:19 2022/8/24
 */

@Slf4j
@RestController
@RequestMapping("/pc/ower")
public class apply_reply {

    @Autowired
    private UserQuery userQuery;
    @Autowired
    private OwerQuery owerQuery;
    @Autowired
    private BuildingQuery buildingQuery;
    @Autowired
    private UserBuildingRepository userBuildingRepository;

    @Autowired
    private OwerApplyRepository owerApplyRepository;

    @Autowired
    private WechatMpUserRepository wechatMpUserRepository;

    /**
     * 查询业主档案-认证处理 - YZDA
     */
    @SaCheckRole(Constants.RoleName.YZDA)
    @VerifyCommunity(true)
    @PostMapping("/apply_reply")
    public Map<String,Object> applyReply(@RequestBody RequestData data) {
        //读取 传递相关参数
        String replyContent = data.getStr("reply_content");
        long communityId = data.getCommunityId();
        long id = data.getId();
        boolean success = data.getBool("success", true, "^0|1$");
        String buildingIdsStr = data.getJsonStr("building_ids");
        JSONArray buildingIds = JSONUtil.parseArray(buildingIdsStr);
        //开始处理逻辑
        ObjData detail = owerQuery.getOwerApplyInfo(communityId,id);
        if (detail == null || detail.getInt("replied") == 1) {
            throw new CodeException(Constants.code.STATUS_ERROR,"非法操作业主认证");
        }

        List<Long> bids = new ArrayList();
        long replied_at = Calendar.getInstance().getTimeInMillis();

        if (success && !JSONUtil.isTypeJSONArray(buildingIdsStr)) {
            throw new CodeException(Constants.code.PARAMS_ERROR, "参数错误");
        }
        if (success) {
            List<ObjData> buildingsInfo = buildingQuery.getBuildInfo(buildingIds);
            if (buildingsInfo.size() == 0) {
                throw new CodeException(Constants.code.NOT_FOUND_BINDING_BUILDING,"未检索到需要关联绑定的住宅信息");
            }
            List<UserBuilding> bindingData = new ArrayList<UserBuilding>();

            for (int i = 0; i < buildingsInfo.size(); i ++) {
                ObjData buildInfo = buildingsInfo.get(i);
                bindingData.add(UserBuilding.builder()
                        .buildingId(buildInfo.getLong("id"))
                        .wechatMpUserId(detail.getLong("wechat_mp_user_id"))
                        .authenticated(1)
                        .authenticatedType(Constants.authenticted.BY_PROPERTY_COMPANY)
                        .authenticatedUserId(Long.valueOf(AuthUtil.getUserId()))
                        .createdAt(System.currentTimeMillis())
                        .build());
                bids.add(buildInfo.getLong("id"));
            }
            //插入相关数据
            List<UserBuilding> s = userBuildingRepository.saveAll(bindingData);
        }
        //更新
        OwerApply owerApply = owerApplyRepository.findByIdAndCommunityId(id,communityId);
        if (owerApply != null) {
            owerApply.setReplyContent(replyContent);
            owerApply.setReplied(1);
            owerApply.setRepliedAt(replied_at);
            owerApply.setContent(JSONUtil.toJsonStr(bids));
            String userName = AuthUtil.getUserId();
            ObjData userInfo = userQuery.getUserInfo(Constants.status.FALSE, userName);
            owerApply.setRepliedBy(userInfo.getLong("id"));
            owerApply.setSuccess(success);
            //更新
            owerApplyRepository.save(owerApply);
        }

        if (success && Boolean.valueOf(detail.getStr("subscribed"))) {
            //查询 wechat_mp_user
            WechatMpUser mpUser = wechatMpUserRepository.findById(detail.getLong("wechat_mp_user_id")).get();
            String openId = mpUser.getOpenId();
            String realName = mpUser.getRealName();

            List<String> ret = new ArrayList<String>();
            if (detail.getStr("house") != null) {
                ret.add(detail.getStr("house"));
            }
            if (detail.getStr("carport") != null) {
                ret.add(detail.getStr("carport"));
            }
            if (detail.getStr("warehouse") != null) {
                ret.add(detail.getStr("warehouse"));
            }
            //微信模板消息
            WeChat.SendSubscribeMessageParams pa = new WeChat.SendSubscribeMessageParams();
            pa.setTouser(openId);
            pa.setTemplate_id(Constants.tpl.MP_OWER_APPROVE);
            pa.setPage("/pages/community/index");
            pa.setData(Map.of("name2",Map.of("value",realName),"thing1",Map.of("value", ToolUtil.omit(String.join(",",ret),16)),"time3",Map.of("value", DateTimeUtil.convertTimestamp2Date(replied_at,"YYYY-MM-dd HH:mm:ss"))));

            WeChat.SendSubscribeMessageResponse mResponse = WeChat.sendMpSubscribeMessage(pa);
            if (null != mResponse && mResponse.getErrcode() != 0) {
                System.out.println("小程序模板" + Constants.tpl.MP_OWER_APPROVE +"推送失败，" + mResponse.getErrmsg());
            }
        }

        List buildings = new ArrayList();

        if (bids.size() > 0) {
            //查询
            buildings = buildingQuery.getBuildings(communityId,detail.getLong("wechat_mp_user_id"),bids);
        }
        return Map.of("replied_at",replied_at,"buildings",buildings);
    }

}
