package cn.chowa.ejyy.user;

import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.UserQuery;
import cn.chowa.ejyy.service.property_company;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/pc/user")
public class info {

    @Autowired
    private UserQuery userQuery;
    @Autowired
    private property_company propertycompany;

    @GetMapping("/info")
    public Map<?, ?> getInfo() {
        return Map.of(
                "userInfo", userQuery.getUserInfo(Constants.status.FALSE, AuthUtil.getUserId()),
                "postInfo", propertycompany.getPostInfo(AuthUtil.getUid())
        );
    }

}
